/**
 * Example
 * new quoteIt(document.getElementById('quote'), {timeout: 4000})
 */
(function(){
  var quotes = ["Life is about making an impact, not making an income.", "Whatever the mind of man can conceive and believe, it can achieve.", "Strive not to be a success, but rather to be of value.", "I attribute my success to this: I never gave or took any excuse.", "You miss 100% of the shots you don’t take.", "I’ve missed more than 9000 shots in my career. I’ve lost almost 300 games. 26 times I’ve been trusted to take the game winning shot and missed. I’ve failed over and over and over again in my life. And that is why I succeed.", "The most difficult thing is the decision to act, the rest is merely tenacity.", "Every strike brings me closer to the next home run.", "Definiteness of purpose is the starting point of all achievement.", "Life isn’t about getting and having, it’s about giving and being.", "Life is what happens to you while you’re busy making other plans.", "We become what we think about.", "wenty years from now you will be more disappointed by the things that you didn’t do than by the ones you did do, so throw off the bowlines, sail away from safe harbor, catch the trade winds in your sails.&nbsp; Explore, Dream, Discover.", "ife is 10% what happens to me and 90% of how I react to it.", "The most common way people give up their power is by thinking they don’t have any.", "The best time to plant a tree was 20 years ago. The second best time is now.", "An unexamined life is not worth living.", "Eighty percent of success is showing up.", "Your time is limited, so don’t waste it living someone else’s life.", "Winning isn’t everything, but wanting to win is.", "I am not a product of my circumstances. I am a product of my decisions.", "Every child is an artist.&nbsp; The problem is how to remain an artist once he grows up.", "You can never cross the ocean until you have the courage to lose sight of the shore.", "I’ve learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel.", "Either you run the day, or the day runs you.", "Whether you think you can or you think you can’t, you’re right.", "The two most important days in your life are the day you are born and the day you find out why.", "Whatever you can do, or dream you can, begin it.&nbsp; Boldness has genius, power and magic in it.", "The best revenge is massive success.", "People often say that motivation doesn’t last. Well, neither does bathing.&nbsp; That’s why we recommend it daily.", "Life shrinks or expands in proportion to one’s courage."];  var quoteIt = function (el, options) {
    this.el = el;
    this.timeout = options.timeout;

    if (options.quotes) {
      this.quotes = quotes.concat([options.quotes]);
    } else {
      this.quotes = quotes;
    }

    this.interval = setInterval(this.changeQuote.bind(this), this.timeout);

    this.changeQuote();
  };

  quoteIt.prototype.changeQuote = function () {
    /**
     * If element is removed from dom clear the interval
     */
    if(!this.el.parentNode) {
      return this.stop();
    }

    var html = "<div class='quote'>" + getRandomQuote(this.quotes) + "</div>";
    this.el.innerHTML = html;
  };

  quoteIt.prototype.stop = function () {
    clearInterval(this.interval);
  };

  function getRandomQuote(quotes) {
    return quotes[getRandomInt(0, quotes.length)];
  }

  /**
   * Returns a random integer between min (included) and max (excluded)
   * Using Math.round() will give you a non-uniform distribution!
   */
  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  if(!window.quoteIt) window.quoteIt = quoteIt;
}());
