/**
 * Usage example
 * $(".quotes").quoteIt({timeout: 4000, quotes: []})
 */
(function ($, undefined) {
  /**
   * Register as jquery plugin
   */
  $.fn.quoteIt = function (options) {
    this.each(function () { // iterate and reformat each matched element
      var el = $(this);
      el.data('quoteIt', new quoteIt(el.get(), options));
    });
    return this;
  };
})(jQuery);
