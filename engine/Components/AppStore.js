import _ from 'lodash';
import Fuse from 'fuse.js';
import assign from 'object-assign';
import BaseStore from './../flux/BaseStore.js';

let state = {
  categories: data.categories,

  //active
  featuredTags: [],
  category: null,
  tag: null,
  items: [],
  activeItem: {},
  years: [],
  allTags: [],

  //ui
  device: "desktop",
  list: false,
  loading: true,
  listOpen: true
};

if (location.pathname === "/" && location.hash) {
  let alias = location.hash.replace("#", "");

  //previous system compatibility
  if (alias.lastIndexOf('wp') === alias.length - 2) {
    alias = alias.substring(0, alias.length - 2);
  }

  //take hash alias
  _.map(state.categories, function (category, name) {
    let item = _.find(category.items, {alias});
    if (item) {
      data.item = alias;
      data.category = name;
    }
  });
}

selectProductCategory(data.category);
selectItem(data.item);

function getItemTags(items) {
  return _.chain(items)
    .map(product => product.tags)
    .flatten()
    .compact()
    .countBy(tag=>tag)
    .map((total, name)=> {
      return {total, name};
    })
    .value();
}

function selectProductCategory(category) {
  state.category = category;
  state.items = state.categories[state.category].items;
  state.allTags = getItemTags(state.items);
  state.featuredTags = state.categories[state.category].featuredTags;
}

function changePageTitle(title) {
  document.querySelector("title").innerHTML = strToTitleCase(title);
}

function strToTitleCase(str) {
  return str.split(" ").map(i=> i[0].toUpperCase() + i.substring(1)).join(" ");
}

function selectItem(alias) {
  let url = data.url + state.category + "/" + alias;

  if (history && history.pushState) {
    history.pushState(null, null, url);
  } else {
    location.hash = alias;
  }

  state.activeItem = _.find(state.items, {alias: alias});
  state.list = false;
  changePageTitle(`${state.activeItem.name} ${state.category} Template - ThemeXpert Demo`);
  previewLoading();
}

function searchItems(context) {
  let items = state.categories[state.category].items;
  context = context.toLowerCase();

  if (!context) {
    return state.items = items;
  }

  let fuse = new Fuse(items, {keys: ["name", "tags", "introtext"]});
  items = fuse.search(context);

  state.items = items;
}


function sortItemsByTag(tag) {
  let items = state.categories[state.category].items;
  state.tag = tag;

  if (!tag) {
    return state.items = items;
  }

  state.items = _.filter(items, item => _.includes(item.tags, tag));
}

function emitChange() {
  AppStore.emitChange();
}

function openDrawerList() {
  AppStore.openDrawerList();
}

function toggleProductsList() {
  state.list = !state.list;
}

function changePreviewDevice(device) {
  state.device = device;
}

function previewLoaded() {
  state.loading = false;
}

function previewLoading() {
  state.loading = true;
}

let AppStore = assign({}, BaseStore, {

  // public methods used by Controller-View to operate on data
  getState() {
    return state;
  },

  toggleProductsList(){
    toggleProductsList();
    openDrawerList();
    emitChange();
  },
  openDrawerList(){
    jQuery('.popup-overlay').toggle();
    jQuery('.tx-demo-toolbar').toggleClass('open');
    jQuery('.drawer').slideToggle(350);
  },
  selectProductCategory(category){
    if (category === state.category) {
      state.list = !state.list;
    } else {
      state.list = true;
    }

    selectProductCategory(category);
    if(state.listOpen == category){
      openDrawerList();
    }else{
      state.listOpen = category;
      jQuery('.drawer').slideDown(350);
      jQuery('.popup-overlay').css('display', 'block');
      jQuery('.tx-demo-toolbar').addClass('open');
    }
    emitChange();
  },

  selectItem(alias){
    selectItem(alias);
    openDrawerList();
    emitChange();
  },

  searchItems(context){
    searchItems(context);
    emitChange();
  },

  sortItemsByTag(tag){
    sortItemsByTag(tag);
    emitChange();
  },

  changePreviewDevice(device){
    changePreviewDevice(device);
    emitChange();
  },

  previewLoaded(){
    previewLoaded();
    emitChange();
  }

});

export default AppStore;
