import React from 'react';

const Button = function (props) {
  return <button onClick={props.onClick} type="button" className={"btn btn-default btn-mini "+props.btnClass}>
    <span className={props.className}></span>
  </button>;
};

export default Button;
