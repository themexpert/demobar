import React from 'react';
import AppStore from '../AppStore.js';
import Button from './Button.jsx';
import Categories from './Categories.jsx';

const Logo = require('babel!svg-react!./assets/logo.svg?name=Logo');

function getCallToAction(productName, price) {
  if (0 === parseInt(price)) {
    return `Download ${productName} - Free`;
  }

  return `Buy ${productName} - \$${price}`;
}

const Navbar = React.createClass({
  propTypes: {
    productName: React.PropTypes.string,
    demoUrl: React.PropTypes.string,
    productUrl: React.PropTypes.string,
    categories: React.PropTypes.object
  },

  getInitialState(){
    return {
      visible: true
    };
  },

  componentDidUpdate(){
    if (this.state.visible) {
      $("body").removeClass("slide-up");
    } else {
      $("body").addClass("slide-up");
    }
  },

  toggleNavbar(e){
    e.preventDefault();
    this.setState({visible: !this.state.visible});
  },

  render() {
    let {productName, demoUrl, productUrl, category, categories, price} = this.props;

    function handleCategoryClick(device) {
      return AppStore.changePreviewDevice.bind(null, device);
    }

    return (
      <nav className="navbar navbar-inverse">

        <div className="row">

          <div className="col-md-3 col-sm-2 col-xs-3">
            <div className="navbar-header">
              <a href="index.php" className="navbar-brand"><Logo /></a>
            </div>
          </div>

          <div className="col-md-8 col-sm-9 col-xs-7">
            <div className="clearfix">
              <Categories active={category} categories={categories} handleClick={AppStore.selectProductCategory}/>

              <h2 className="previewProduct" onClick={AppStore.toggleProductsList}>
                {productName}
                <span className="carets pull-right">
                  <i className="fa fa-caret-up"/>
                  <i className="fa fa-caret-down"/>
                </span>
              </h2>

              <a href={productUrl} className="btn-buynow btn btn-primary hidden-sm hidden-xs">
                {getCallToAction(productName, price)}
              </a>

              <a href="https://www.themexpert.com/pricing" className="btn btn-success hidden-xs">
                Join Club
              </a>

              <div className="responsive-btn-wrapper btn-group hidden-sm hidden" role="group">
                <Button onClick={handleCategoryClick("desktop")} className="fa fa-desktop"/>
                <Button onClick={handleCategoryClick("tablet-portrait")} className="fa fa-tablet fa-rotate-90"/>
                <Button onClick={handleCategoryClick("tablet")} className="fa fa-tablet"/>
                <Button onClick={handleCategoryClick("phone-portrait")} className="fa fa-mobile-phone fa-rotate-90"/>
                <Button onClick={handleCategoryClick("phone")} className="fa fa-mobile-phone"/>
              </div>

            </div>
          </div>

          <div className="col-md-1 col-sm-1 col-xs-2">
            <div className="close-wrapper">
              <a href={demoUrl} onClick={this.toggleNavbar} type="button"
                 className="btn-close btn btn-danger pull-right"><i className="fa fa-close"></i></a>
            </div>
          </div>

        </div>
        <a href="javascript::void();" onClick={this.toggleNavbar} 
          className={`btn btn-danger angle-down ${!this.state.visible || "hide"}`}>
          <span className="fa fa-angle-down"></span>
        </a>
      </nav>
    );
  }
});

export default Navbar;
