import cx from 'classnames';
import React from 'react';
import Button from './Button.jsx';
import _ from 'lodash';

const Categories = function (props) {
  let {active, handleClick, categories} = props;

  function renderCategory(category, alias){
    return <Button key={alias} btnClass={cx({"active": active === alias})} className={category.icon} onClick={handleClick.bind(null, alias)} />
  }

  return <div className="category-selector btn-group" role="group">
    {_.map(categories, renderCategory) }
  </div>;
};

export default Categories;
