import React from 'react';
import {findDOMNode} from 'react-dom';

import './carousel.less';

function carouselMarkup(products) {
  let template = _.template(
    `<div class="item thumbnail" data-product-alias="<%=product.alias%>" >
      <%if(product.hits > 5000){%>
        <span class="label label-info hot">Hot</span>
      <%}%>
      <img src="<%=product.images%>"/>
      <h3><%=product.name%></h3>
    </div>`
  );

  return products.map(product=>template({product})).join("\n");
}

const Carousel = React.createClass({
  propTypes: {
    products: React.PropTypes.array,
    onClick: React.PropTypes.func
  },

  bindItemClick: function () {
    let {onClick} = this.props;

    $(document).on("click", "[data-product-alias]", function () {
      let alias = $(this).attr('data-product-alias');
      onClick(alias);
    });

  },

  componentDidMount() {
    this.mountCarousel();
    this.bindItemClick();
  },

  componentDidUpdate(){
    this.mountCarousel();
  },

  componentWillUpdate(){
    this.unmountCarousel();
  },

  unmountCarousel(){
    let selector = `.owl-carousel`;

    // $(selector).data('owl.carousel').destroy();
    $(selector).owlCarousel({touchDrag: false, mouseDrag: false});
  },

  mountCarousel() {
    let {products} = this.props;

    let html = `<div class="owl-carousel owl-theme">` + carouselMarkup(products) + "</div>";
    let selector = `.owl-carousel`;

    let options = {
      dot: true,
      nav: true,
      navText: [
        '<i class="fa fa-chevron-left"/>',
        '<i class="fa fa-chevron-right"/>',
      ],
      lazyLoad: !true,
      responsive: {
        0: { items: 2},
        600: { items: 3},
        1000: { items: 4}
      },
      margin : 10
    };

    // $(findDOMNode(this)).html(html);
    $(findDOMNode(this)).hide().html(html).fadeIn("720");
    $(selector).owlCarousel(options);
  },

  render() {
    return <div className="drawer-carousel" />;
  }
});

export default Carousel;
