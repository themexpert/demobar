import React from 'react';
import Link from './Link.jsx';
import cx from 'classnames';

function toggleDropDown() {
  $('.see-more').toggleClass('open');
}

const Tags = React.createClass({
  propTypes: {
    //[{name, total}, {name, total}]
    tags: React.PropTypes.array,
    onClick: React.PropTypes.func
  },

  componentDidMount() {
    $(document).on('click', function (event) {
      if ($('.see-more').hasClass('open') && !$(event.target).closest('.see-more').length) {
        toggleDropDown();
      }
    });
  },


  handleClick(tag){
    toggleDropDown();
    this.props.onClick(tag);
  },

  _renderList(data){
    let {total, name} = data;
    let {tag} = this.props;
    let active = tag === name;

    return (
      <Link key={name} text={name} total={total} active={active} onClick={this.handleClick.bind(null, name)}/>
    );
  },

  render() {
    let {allTags, tag, featuredTags} = this.props;
    featuredTags = featuredTags.concat([tag]);

    return (
      <ul className="nav navbar-nav hidden-xs">
        <li role="presentation" className={cx({active: !tag})}>
          <a onClick={this.handleClick.bind(null, undefined)} href="#">All</a>
        </li>

        {allTags.filter(tag=>_.includes(featuredTags, tag.name)).map(this._renderList)}

        <li role="presentation" className="see-more">
          <a href="#" data-toggle="dropdown" onClick={toggleDropDown}>
            More <i className="fa fa-caret-down"></i>
          </a>

          <ul className="dropdown-menu">
            { allTags.map(this._renderList)}
          </ul>
        </li>
      </ul>
    );
  }
});

export default Tags;
