import React from 'react';
import cx from 'classnames';

const LabelInfo = function(props){
  return <span className="label label-info">{props.content}</span>;
};

const Link = function (props) {
  let {onClick, text, total, active} = props;

  return (
    <li role="presentation" className={cx({active})}>
      <a onClick={onClick} href="#">{text} <LabelInfo content={total}/></a>
    </li>
  );
};

export default Link;
