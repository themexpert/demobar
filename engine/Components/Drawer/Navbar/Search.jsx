import React from 'react';

const Search = React.createClass({
  propTypes: {
    handleSearch: React.PropTypes.func
  },

  _handleSearch(e){
    let context = e.target.value;
    let {handleSearch} = this.props;

    handleSearch(context);
  },

  render() {
    return (
      <form className="navbar-form navbar-right" role="search">
        <div className="form-group">
          <div className="input-group">
            <div className="input-group-addon"><span className="fa fa-search"></span></div>
            <input type="text" className="form-control input-sm" onKeyUp={this._handleSearch} placeholder="Search"/>
          </div>
        </div>
      </form>
    );
  }
});

export default Search;
