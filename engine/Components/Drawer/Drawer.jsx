import React from 'react';
import cx from 'classnames';
import _ from 'lodash';

import Search from './Navbar/Search.jsx';
import Tags from './Navbar/Tags.jsx';
import Carousel from './Carousel/Carousel.jsx';
import AppStore from './../AppStore.js';

const List = React.createClass({
  propTypes: {
    products: React.PropTypes.array,
    allTags: React.PropTypes.array,
    tag: React.PropTypes.string,
    featuredTags: React.PropTypes.array,
    show: React.PropTypes.bool
  },

  _handleTagClick(tag){
    AppStore.sortItemsByTag(tag);
  },

  _handleTemplateClick(alias){
    AppStore.selectItem(alias);
  },

  render() {
    let {products, allTags, tag, featuredTags} = this.props;

    return (
      <div className="drawer">
        <div className="navbar navbar-default hidden-xs">
          <Tags featuredTags={featuredTags} allTags={allTags} tag={tag} onClick={this._handleTagClick}/>
          <Search handleSearch={AppStore.searchItems}/>
        </div>

        <Carousel products={products} onClick={this._handleTemplateClick}></Carousel>
      </div>
    );
  }
});

export default List;
