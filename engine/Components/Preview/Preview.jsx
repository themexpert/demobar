import React from 'react';
import cx from 'classnames';
import AppStore from '../AppStore.js';
import Spinner from './Spinner/Spinner.jsx';

import "./style.less";

function iframeHeight() {
  return $(window).height() - $(".tx-demo-toolbar").height();
}

const Preview = React.createClass({
  propTypes: {
    demo_url: React.PropTypes.string,
    device: React.PropTypes.string
  },

  componentDidMount() {
    this.setIframeHeight();
    this.setIframeSpinner();
  },

  setIframeSpinner(){
    $(this.refs.iframe).on("load", AppStore.previewLoaded);
  },

  setIframeHeight(){
    $(this.refs.iframe).css("height", iframeHeight());

    $(window).on("resize", ()=> {
      $(this.refs.iframe).css("height", iframeHeight());
    });
  },

  render() {
    let {loading, demo_url, device} = this.props;
    let classes = cx("preview-frame", device);

    return (
      <div className="preview-wrapper">
        <div className="popup-overlay"></div>
        <Spinner loading={loading}/>
        <iframe ref="iframe" className={classes} src={demo_url} frameBorder="0"></iframe>
      </div>
    );
  }
});

export default Preview;
