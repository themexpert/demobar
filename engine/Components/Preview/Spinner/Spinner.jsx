import React from "react";
import "./spinner.less";

const Spinner = React.createClass({
  propTypes: {
    loading: React.PropTypes.bool
  },
  componentDidMount() {
    // new quoteIt(this.refs.quotes, {
    //   timeout: 4000, quotes: []
    // });
  },


  render() {
    if (!this.props.loading) {
      return <span />;
    }

    return (
      <div className="overlay">
        <div className='spinner-body'>
          <span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </span>
          <div className='base'>
            <span></span>
            <div className='face'></div>
          </div>
        </div>
        <div className='longfazers'>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    );
  }
});

export default Spinner;
