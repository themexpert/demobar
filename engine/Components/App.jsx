import React from 'react';
import AppStore from "./AppStore.js";

import Preview from "./Preview/Preview.jsx";
import Drawer from "./Drawer/Drawer.jsx";
import Navbar from './Navbar/Navbar.jsx'

import './style.less';

let App = React.createClass({
  getInitialState() {
    return AppStore.getState();
  },

  _onChange() {
    this.setState(AppStore.getState());
  },

  componentDidMount() {
    AppStore.addChangeListener(this._onChange);
  },

  _showIf(list, component) {
    return list ? component : component;
  },

  render() {
    let {items, list, activeItem, allTags, tag, category, categories, featuredTags, device, loading} = this.state;

    return (
      <div>
        <header className="tx-demo-toolbar">
          <Navbar
            categories={categories}
            category={category}
            productName={activeItem.name}
            demoUrl={activeItem.demo_url}
            price={activeItem.price}
            productUrl={activeItem.cart_url}/>

          {this._showIf(list,
          <Drawer featuredTags={featuredTags} tag={tag} allTags={allTags} show={list} products={items}/>)}
        </header>

        <Preview loading={loading} device={device} demo_url={activeItem.demo_url}/>
      </div>
    );
  }
});

export default App;
