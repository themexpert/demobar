<?php
$file    = __DIR__ . '/' . $_SERVER['REQUEST_URI'];
if ( file_exists( $file ) ) {
  return false; // serve the requested resource as-is.
} else {
  include_once 'index.php';
}
