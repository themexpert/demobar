<?php

if(!function_exists('view')){
  /**
   * @param $file
   * @param $data
   *
   * @return string
   */
  function view($file, $data){
    // print_r($data); die();
    extract($data);

    ob_start();
    require $file;
    $output = ob_get_contents();
    ob_end_clean();

    return $output;
  }

}

if(!function_exists('dd')){
  /**
   * @param $var
   * @param bool|true $kill
   */
  function dd($var, $kill=true){
    echo "<pre>";
    var_dump($var);
    $kill ? die(): "</pre>";
  };

}

if (!function_exists('pd')) {
  /**
   * @param $var
   * @param bool|true $kill
   */
  function pd($var, $kill=true){
    echo "<pre>";
    print_r($var);
    $kill ? die(): "</pre>";
  };

}

function url($var=""){
  $http = isSecure() ? "https://" : "http://";
  $domain = $_SERVER['SERVER_NAME'];
  $port = $_SERVER['SERVER_PORT'] === "80" || isSecure() ? "":":{$_SERVER['SERVER_PORT']}";

  return $http.$domain.$port."/".$var;
}

function isSecure() {
  return ( ! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;
}


if ( ! function_exists( 'array_find_by' ) ) {
  function array_find_by( $collection, $key, $value ) {
    $foundKey = array_search( $value, array_column( $collection, $key ) );
    if(is_bool($foundKey)) return false;
    return array_key_exists( $foundKey, $collection ) ? $collection[ $foundKey ] : false;
  }
}
