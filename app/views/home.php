<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo $title ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <?php foreach ( $metadata as $meta => $content ): ?>
    <meta name="<?php echo $meta ?>" content="<?php echo $content ?>"/>
  <?php endforeach; ?>

  <link href="https://www.themexpert.com/templates/txv5/favicon.ico" rel="shortcut icon"
        type="image/vnd.microsoft.icon"/>

  <script>if ( top !== self ) top.location.replace( self.location.href );// Hey, don't iframe my iframe!</script>
  
  <link rel="stylesheet" href="<?php echo url( 'assets/css/bootstrap.css' ) ?>"/>
  <link rel="stylesheet" href="<?php echo url( 'assets/css/font-awesome.css' ) ?>"/>
  <link rel="stylesheet" href="<?php echo url( 'assets/css/owl.carousel.css' ) ?>"/>
  <link rel="stylesheet" href="<?php echo url( 'assets/css/owl.theme.css' ) ?>"/>
  <link rel="stylesheet" href="<?php echo url( 'assets/css/style.css' ) ?>"/>
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto&subset=latin,latin-ext" type="text/css"
        media="screen"/>


  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-M8J78L');</script>
  <!-- End Google Tag Manager -->
</head>
<body>

<div id="root">
  <div class="overlay">
    <div class='spinner-body'>
      <span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </span>
      <div class='base'>
        <span></span>
        <div class='face'></div>
      </div>
    </div>
    <div class='longfazers'>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
    <!--<h1>Loading...</h1>-->
  </div>
</div>

<script>
  var data = <?php echo json_encode($data) ?>;
</script>

<script src="<?php echo url( 'assets/js/jquery.js' ) ?>"></script>
<script src="<?php echo url( 'assets/js/owl.carousel.js' ) ?>"></script>
<script src="<?php echo url( 'assets/bundle.js' ) ?>"></script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M8J78L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<script>if ( top !== self ) top.location.replace( self.location.href );// Hey, don't iframe my iframe!</script>
</body>
</html>
