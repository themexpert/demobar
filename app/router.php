<?php

$klein = new \Klein\Klein();

$klein->respond( '/?[:category]?/[:item]?', function ( $request ) {
  global $config;
  $data = $config;

  if ( isset( $request->category ) && in_array( $request->category, array_keys( $data['categories'] ) ) ) {
    $data['category'] = $request->category;
  }

  $items = $data['categories'][$data['category']]['items'];

  if ( isset( $request->item ) ) {
    $item = array_find_by( $items, 'alias', $request->item ) ?: $items[0];
  } else {
    $ii = 0;
    $item = $items[$ii ++];

    /** do not show a preorder item by default */
    while ( isset( $item['preorder'] ) && $item['preorder'] === 1 ) {
      $item = $items[$ii ++];
    }
  }

  $data['item'] = $item['alias'];
  $title = ucwords( sprintf( "%s %s Template - ThemeXpert Demo", $item['name'], $data['category'] ) );
  $metadata = [
    'author' => 'Parvez Akther',
    'rights'=>"ThemeXpert",
    'og:title' => $title,
    'og:image' => $item['images'],
    'og:url' => $item['url'],
    //https://developers.facebook.com/docs/reference/opengraph/object-type/product.item/
    'og:type' => 'product.item',
    'og:description' => $item['introtext'],
    'description' => $item['introtext'],
    'keywords' => strtolower( implode( ", ", array_map( function ( $tag ) use ( $data ) {
      return sprintf( "%s %s Template", $tag, $data['category'] );
    }, $item['tags'] ) ) )
  ];

  return view( 'views/home.php', compact( 'title', 'metadata', 'data' ) );
} );

$klein->dispatch();
