<?php
$cache_dir    = __DIR__ . "/cache";
$wp_cache     = "{$cache_dir}/wp.json";
$joomla_cache = "{$cache_dir}/joomla.json";

if ( isset( $_GET['reset'] ) ) {
  exec( "rm -rf {$cache_dir}" );
}

if ( ! file_exists( $cache_dir ) ) {
  mkdir( $cache_dir );
}

if ( ! file_exists( $wp_cache ) ) {
  $wp_api = "https://www.themexpert.com/index.php?option=com_digicom&task=responses&source=authapi&catid=28";
  $wp     = file_get_contents( $wp_api );

  file_put_contents( $wp_cache, $wp );
}

if ( ! file_exists( $joomla_cache ) ) {
  $joomla_api = "https://www.themexpert.com/index.php?option=com_digicom&task=responses&source=authapi&catid=26";
  $joomla     = file_get_contents( $joomla_api );

  file_put_contents( $joomla_cache, $joomla );
}
