<?php
$wp     = file_get_contents( $wp_cache );
$joomla = file_get_contents( $joomla_cache );

return array(
  "url"        => url(),
  "category"   => "joomla",
  "categories" => [
    "wordpress" => [
      "name"         => "WordPress",
      "icon"         => "fa fa-wordpress",
      "items"        => array_values( json_decode( $wp, true )['data'] ),
      "featuredTags" => [ "Onepage", "Business", "Magazine", "Portfolio" ],
    ],
    "joomla"    => [
      "name"         => "joomla",
      "icon"         => "fa fa-joomla",
      "items"        => array_values( json_decode( $joomla, true )['data'] ),
      "featuredTags" => [ "Business", "Multipurpose", "Ecommerce", "Education", "App", "Gaming", "Music" ],
    ],
  ],
);
