# How to build
```sh
composer install
npm install gulp webpack bower -g
npm install
bower install
gulp
```

# How to reset cache
[url]?reset=true

# How to build
gulp package-build
