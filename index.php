<?php
error_reporting( 0 );
error_reporting( E_ALL );
ini_set( 'error_reporting', E_ALL );

require "vendor/autoload.php";
require "app/functions.php";
require "app/cache.php";
$config = require "app/config.php";
require "app/router.php";
